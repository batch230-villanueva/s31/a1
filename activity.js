const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {
    switch(request.url) {
        case '/login':
            response.writeHead(200, {'Content-Type' : 'text/plain'});
            response.end("Welcome to the Log In page.");
            break;
        default: 
            response.writeHead(404, {'Content-Type' : 'text/plain'});
            response.end("I'm sorry the page you are looking for cannot be found.");
            break;
    }
});

server.listen(port);

console.log(`The server is running at port ${port}`);